# translation of kcmkeyboard.po to Lithuanian
# This file is distributed under the same license as the kcmkeyboard package.
# Gintautas Miselis <gintautas@miselis.lt>, 2008.
# Tomas Straupis <tomasstraupis@gmail.com>, 2010, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Liudas Alisauskas <liudas@akmc.lt>, 2013, 2015.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-03 01:37+0000\n"
"PO-Revision-Date: 2024-01-13 11:56+0200\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.0.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Andrius Štikonas, Moo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "andrius@stikonas.eu, <>"

#: bindings.cpp:24
#, kde-format
msgid "Keyboard Layout Switcher"
msgstr "Klaviatūros išdėstymų perjungiklis"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "Perjungti į kitą klaviatūros išdėstymą"

#: bindings.cpp:30
#, kde-format
msgid "Switch to Last-Used Keyboard Layout"
msgstr "Perjungti į paskiausiai naudotą klaviatūros išdėstymą"

#: bindings.cpp:60
#, kde-format
msgid "Switch keyboard layout to %1"
msgstr "Perjungti klaviatūros išdėstymą į %1"

#: flags.cpp:77
#, kde-format
msgctxt "layout - variant"
msgid "%1 - %2"
msgstr "%1 – %2"

#. i18n: ectx: property (windowTitle), widget (QDialog, AddLayoutDialog)
#: kcm_add_layout_dialog.ui:14
#, kde-format
msgid "Add Layout"
msgstr "Pridėti išdėstymą"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, layoutSearchField)
#: kcm_add_layout_dialog.ui:20
#, kde-format
msgid "Search…"
msgstr "Ieškoti…"

#. i18n: ectx: property (text), widget (QLabel, shortcutLabel)
#: kcm_add_layout_dialog.ui:45
#, kde-format
msgid "Shortcut:"
msgstr "Spartusis klavišas:"

#. i18n: ectx: property (text), widget (QLabel, labelLabel)
#: kcm_add_layout_dialog.ui:55
#, kde-format
msgid "Label:"
msgstr "Pavadinimas:"

#. i18n: ectx: property (text), widget (QPushButton, prevbutton)
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm_add_layout_dialog.ui:76 kcm_keyboard.ui:341
#, kde-format
msgid "Preview"
msgstr "Peržiūra"

#. i18n: ectx: attribute (title), widget (QWidget, tabHardware)
#: kcm_keyboard.ui:21
#, kde-format
msgid "Hardware"
msgstr "Aparatinė įranga"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm_keyboard.ui:36
#, kde-format
msgid "Keyboard &model:"
msgstr "Klaviatūros &modelis:"

#. i18n: ectx: property (whatsThis), widget (QComboBox, keyboardModelComboBox)
#: kcm_keyboard.ui:56
#, kde-format
msgid ""
"Here you can choose a keyboard model. This setting is independent of your "
"keyboard layout and refers to the \"hardware\" model, i.e. the way your "
"keyboard is manufactured. Modern keyboards that come with your computer "
"usually have two extra keys and are referred to as \"104-key\" models, which "
"is probably what you want if you do not know what kind of keyboard you "
"have.\n"
msgstr ""
"Čia galite pasirinkti klaviatūros modelį. Ši nuostata yra nepriklausomas nuo "
"jūsų klaviatūros išdėstymo. Jis tiesiog nurodo klaviatūros \"aparatinės "
"įrangos\" modelį, t.y. gamintojo nurodomą tipą. Šiuolaikinėse klaviatūrose, "
"dažniausiai, yra pridėti du papildomi klavišai, todėl jos vadinamos „104 "
"klavišų“ modeliais. Būtent šį modelį ir galite pasirinkti, jei nežinote "
"tikslaus savo klaviatūros modelio.\n"

#. i18n: ectx: attribute (title), widget (QWidget, tabLayouts)
#: kcm_keyboard.ui:97
#, kde-format
msgid "Layouts"
msgstr "Išdėstymai"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:105
#, kde-format
msgid ""
"If you select \"Application\" or \"Window\" switching policy, changing the "
"keyboard layout will only affect the current application or window."
msgstr ""
"Jei pasirenkate perjungimo politiką „Programoje“ arba „Lange“, klaviatūros "
"išdėstymo keitimas veiks tik esamoje programoje ar esamame lange."

#. i18n: ectx: property (title), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:108
#, kde-format
msgid "Switching Policy"
msgstr "Perjungimo politika"

#. i18n: ectx: property (text), widget (QRadioButton, switchByGlobalRadioBtn)
#: kcm_keyboard.ui:114
#, kde-format
msgid "&Global"
msgstr "&Globaliai"

#. i18n: ectx: property (text), widget (QRadioButton, switchByDesktopRadioBtn)
#: kcm_keyboard.ui:127
#, kde-format
msgid "&Desktop"
msgstr "&Darbalaukyje"

#. i18n: ectx: property (text), widget (QRadioButton, switchByApplicationRadioBtn)
#: kcm_keyboard.ui:137
#, kde-format
msgid "&Application"
msgstr "&Programoje"

#. i18n: ectx: property (text), widget (QRadioButton, switchByWindowRadioBtn)
#: kcm_keyboard.ui:147
#, kde-format
msgid "&Window"
msgstr "&Lange"

#. i18n: ectx: property (title), widget (QGroupBox, shortcutsGroupBox)
#: kcm_keyboard.ui:160
#, kde-format
msgid "Shortcuts for Switching Layout"
msgstr "Išdėstymo perjungimo spartieji klavišai"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm_keyboard.ui:166
#, kde-format
msgid "Main shortcuts:"
msgstr "Pagrindiniai spartieji klavišai:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkbGrpShortcutBtn)
#: kcm_keyboard.ui:179
#, kde-format
msgid ""
"This is a shortcut for switching layouts which is handled by X.org. It "
"allows modifier-only shortcuts."
msgstr ""
"Tai yra X.org valdomas išdėstymo perjungimo spartusis klavišas. Čia galima "
"naudoti sparčiuosius klavišus vien iš modifikavimo klavišų."

#. i18n: ectx: property (text), widget (QPushButton, xkbGrpShortcutBtn)
#. i18n: ectx: property (text), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:182 kcm_keyboard.ui:212
#, kde-format
msgctxt "no shortcut defined"
msgid "None"
msgstr "Nėra"

#. i18n: ectx: property (text), widget (QToolButton, xkbGrpClearBtn)
#. i18n: ectx: property (text), widget (QToolButton, xkb3rdLevelClearBtn)
#: kcm_keyboard.ui:189 kcm_keyboard.ui:219
#, kde-format
msgid "…"
msgstr "…"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm_keyboard.ui:196
#, kde-format
msgid "3rd level shortcuts:"
msgstr "3 lygmens spartieji klavišai:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:209
#, kde-format
msgid ""
"This is a shortcut for switching to a third level of the active layout (if "
"it has one) which is handled by X.org. It allows modifier-only shortcuts."
msgstr ""
"Tai yra X.org valdomas spartusis klavišas, naudojamas perjungimui į trečiąjį "
"išdėstymo lygmenį (jei toks yra). Čia galima naudoti sparčiuosius klavišus "
"vien iš modifikavimo klavišų."

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm_keyboard.ui:226
#, kde-format
msgid "Alternative shortcut:"
msgstr "Alternatyvus spartusis klavišas:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, kdeKeySequence)
#: kcm_keyboard.ui:239
#, kde-format
msgid ""
"This is a shortcut for switching layouts. It does not support modifier-only "
"shortcuts and also may not work in some situations (e.g. if popup is active "
"or from screensaver)."
msgstr ""
"Tai yra išdėstymo perjungimo spartusis klavišas. Čia negalima naudoti vien "
"iš modifikavimo klavišų sudarytus sparčiuosius klavišus. Jis taipogi neveiks "
"kai kuriose situacijose (pavyzdžiui esant aktyviam iškylančiajam langui arba "
"ekrano užsklandoje)."

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm_keyboard.ui:249
#, kde-format
msgid "Last used shortcut:"
msgstr "Paskiausiai naudoto spartusis klavišas:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, toggleLastUsedLayoutKeySequence)
#: kcm_keyboard.ui:262
#, kde-format
msgid ""
"This shortcut allows for fast switching between two layouts, by always "
"switching to the last-used one."
msgstr ""
"Šis spartusis klavišas leidžia greitai perjungti tarp dviejų išdėstymų, "
"visada perjungdamas į paskiausiai naudotą išdėstymą."

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_configureLayouts)
#: kcm_keyboard.ui:287
#, kde-format
msgid "Configure layouts"
msgstr "Konfigūruoti išdėstymus"

#. i18n: ectx: property (text), widget (QPushButton, addLayoutBtn)
#: kcm_keyboard.ui:301
#, kde-format
msgid "Add"
msgstr "Pridėti"

#. i18n: ectx: property (text), widget (QPushButton, removeLayoutBtn)
#: kcm_keyboard.ui:311
#, kde-format
msgid "Remove"
msgstr "Šalinti"

#. i18n: ectx: property (text), widget (QPushButton, moveUpBtn)
#: kcm_keyboard.ui:321
#, kde-format
msgid "Move Up"
msgstr "Pakelti"

#. i18n: ectx: property (text), widget (QPushButton, moveDownBtn)
#: kcm_keyboard.ui:331
#, kde-format
msgid "Move Down"
msgstr "Nuleisti"

#. i18n: ectx: property (text), widget (QCheckBox, layoutLoopingCheckBox)
#: kcm_keyboard.ui:376
#, kde-format
msgid "Spare layouts"
msgstr "Atsarginiai išdėstymai"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm_keyboard.ui:408
#, kde-format
msgid "Main layout count:"
msgstr "Pagrindinių išdėstymų skaičius:"

#. i18n: ectx: attribute (title), widget (QWidget, tabAdvanced)
#: kcm_keyboard.ui:438
#, kde-format
msgid "Advanced"
msgstr "Išplėstiniai"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_resetOldXkbOptions)
#: kcm_keyboard.ui:444
#, kde-format
msgid "&Configure keyboard options"
msgstr "&Konfigūruoti klaviatūros parinktis"

#: kcm_keyboard_widget.cpp:209
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr "Nežinomas"

#: kcm_keyboard_widget.cpp:211
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr "%1 | %2"

#: kcm_keyboard_widget.cpp:655
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr "Nėra"

#: kcm_keyboard_widget.cpp:669
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] "%1 spartusis klavišas"
msgstr[1] "%1 spartieji klavišai"
msgstr[2] "%1 sparčiųjų klavišų"
msgstr[3] "%1 spartusis klavišas"

# Pavadinimas stulpelio, kuriame yra išdėstymų trumpiniai "lt", "us", "de" ir t.t.
#: kcm_view_models.cpp:200
#, kde-format
msgctxt "layout map name"
msgid "Map"
msgstr "Schema"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Layout"
msgstr "Išdėstymas"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Variant"
msgstr "Variantas"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Label"
msgstr "Pavadinimas"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Shortcut"
msgstr "Spartusis klavišas"

#: kcm_view_models.cpp:273
#, kde-format
msgctxt "variant"
msgid "Default"
msgstr "Numatytasis"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcmmiscwidget.ui:31
#, kde-format
msgid "When a key is held:"
msgstr "Laikant klavišą nuspaustą:"

#. i18n: ectx: property (text), widget (QRadioButton, accentMenuRadioButton)
#: kcmmiscwidget.ui:38
#, kde-format
msgid "&Show accented and similar characters "
msgstr "&Rodyti simbolius su kirčiais ir pan. "

#. i18n: ectx: property (text), widget (QRadioButton, repeatRadioButton)
#: kcmmiscwidget.ui:45
#, kde-format
msgid "&Repeat the key"
msgstr "&Kartoti klavišą"

#. i18n: ectx: property (text), widget (QRadioButton, nothingRadioButton)
#: kcmmiscwidget.ui:52
#, kde-format
msgid "&Do nothing"
msgstr "&Nieko nedaryti"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcmmiscwidget.ui:66
#, kde-format
msgid "Test area:"
msgstr "Vieta pabandymui:"

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEdit)
#: kcmmiscwidget.ui:73
#, kde-format
msgid ""
"Allows to test keyboard repeat and click volume (just don't forget to apply "
"the changes)."
msgstr ""
"Leidžia išbandyti klaviatūros kartojimus ir paspaudimų garsumą (tik "
"nepamirškite pritaikyti pakeitimus)."

#. i18n: ectx: property (whatsThis), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:82
#, kde-format
msgid ""
"If supported, this option allows you to setup the state of NumLock after "
"Plasma startup.<p>You can configure NumLock to be turned on or off, or "
"configure Plasma not to set NumLock state."
msgstr ""
"Jei palaikoma, ši parinktis leidžia nustatyti skaitmenų klaviatūros būseną "
"po Plasma paleidimo.<p>Galite nustatyti, kad skaitmenų klaviatūra būtų "
"įjungta arba išjungta, arba nustatyti, kad Plasma nekeistų skaitmenų "
"klaviatūros būsenos."

#. i18n: ectx: property (title), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:85
#, kde-format
msgid "NumLock on Plasma Startup"
msgstr "Skaitmenų klaviatūros būsena paleidžiant KDE Plasma"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton1)
#: kcmmiscwidget.ui:97
#, kde-format
msgid "T&urn on"
msgstr "Į&jungti"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton2)
#: kcmmiscwidget.ui:104
#, kde-format
msgid "&Turn off"
msgstr "&Išjungti"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton3)
#: kcmmiscwidget.ui:111
#, kde-format
msgid "Leave unchan&ged"
msgstr "&Nekeisti"

#. i18n: ectx: property (text), widget (QLabel, lblRate)
#: kcmmiscwidget.ui:148
#, kde-format
msgid "&Rate:"
msgstr "&Sparta:"

#. i18n: ectx: property (whatsThis), widget (QSlider, delaySlider)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:164 kcmmiscwidget.ui:202
#, kde-format
msgid ""
"If supported, this option allows you to set the delay after which a pressed "
"key will start generating keycodes. The 'Repeat rate' option controls the "
"frequency of these keycodes."
msgstr ""
"Jei palaikoma, ši parinktis leidžia nustatyti delsą, po kurios nuspaustas "
"klavišas pradės generuoti simbolius. Kartojimo spartos parinktis nustato, "
"kokiu dažniu bus generuojami simboliai."

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, kcfg_repeatRate)
#. i18n: ectx: property (whatsThis), widget (QSlider, rateSlider)
#: kcmmiscwidget.ui:192 kcmmiscwidget.ui:212
#, kde-format
msgid ""
"If supported, this option allows you to set the rate at which keycodes are "
"generated while a key is pressed."
msgstr ""
"Jei palaikoma, ši parinktis leidžia nustatyti spartą, kuria generuojami "
"simboliai nuspaudus klavišą."

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_repeatRate)
#: kcmmiscwidget.ui:195
#, kde-format
msgid " repeats/s"
msgstr " kartojimų/sek."

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:205
#, kde-format
msgid " ms"
msgstr " ms"

#. i18n: ectx: property (text), widget (QLabel, lblDelay)
#: kcmmiscwidget.ui:246
#, kde-format
msgid "&Delay:"
msgstr "&Delsa:"

#: tastenbrett/main.cpp:52
#, kde-format
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "Klaviatūros peržiūra"

#: tastenbrett/main.cpp:54
#, kde-format
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "Klaviatūros išdėstymų atvaizdavimas"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant."
msgstr ""
"Nepavyko įkelti klaviatūros geometrijos. Dažnu atveju tai nurodo į tai, kad "
"pasirinktas modelis nepalaiko tam tikro išdėstymo ar išdėstymo varianto. Ši "
"problema, greičiausiai, atsiranda, kai bandote naudoti šio modelio, "
"išdėstymo ir varianto kombinaciją."

#~ msgid "KDE Keyboard Control Module"
#~ msgstr "KDE Klaviatūros valdymo modulis"

#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "Autorių teisės (c) 2010 Andriy Rysin"

#~ msgid ""
#~ "<h1>Keyboard</h1> This control module can be used to configure keyboard "
#~ "parameters and layouts."
#~ msgstr ""
#~ "<h1>Klaviatūra</h1> Šio valdymo modulio pagalba galima konfigūruoti "
#~ "klaviatūros parametrus ir išdėstymus."

#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "KDE klaviatūros išdėstymų perjungiklis"

#~ msgid "Only up to %1 keyboard layout is supported"
#~ msgid_plural "Only up to %1 keyboard layouts are supported"
#~ msgstr[0] "Palaikomas tik %1 klaviatūros išdėstymas"
#~ msgstr[1] "Palaikoma tik iki %1 klaviatūros išdėstymų"
#~ msgstr[2] "Palaikoma tik iki %1 klaviatūros išdėstymų"
#~ msgstr[3] "Palaikoma tik iki %1 klaviatūros išdėstymų"

#~ msgid "Any language"
#~ msgstr "Bet kokia kalba"

#~ msgid "Layout:"
#~ msgstr "Išdėstymas:"

#~ msgid "Variant:"
#~ msgstr "Variantas:"

#~ msgid "Limit selection by language:"
#~ msgstr "Riboti pasirinkimą pagal kalbą:"

#~ msgid "..."
#~ msgstr "..."

#~ msgctxt "short layout label - full layout name"
#~ msgid "%1 - %2"
#~ msgstr "%1 – %2"

#~ msgid "Layout Indicator"
#~ msgstr "Išdėstymo indikatorius"

#~ msgid "Show layout indicator"
#~ msgstr "Rodyti išdėstymo indikatorių"

#~ msgid "Show for single layout"
#~ msgstr "Rodyti net vieninteliam išdėstymui"

#~ msgid "Show flag"
#~ msgstr "Rodyti vėliavą"

#~ msgid "Show label"
#~ msgstr "Rodyti pavadinimą"

#~ msgid "Show label on flag"
#~ msgstr "Rodyti pavadinimą ant vėliavos"

#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "Klaviatūros išdėstymas"

#~ msgid "Configure Layouts..."
#~ msgstr "Konfigūruoti išdėstymus..."

#~ msgid "Keyboard Repeat"
#~ msgstr "Klavišo kartojimas"

#~ msgid "Turn o&ff"
#~ msgstr "&Išjungti"

#~ msgid "&Leave unchanged"
#~ msgstr "&Nekeisti"

#~ msgid "Configure..."
#~ msgstr "Konfigūruoti..."
