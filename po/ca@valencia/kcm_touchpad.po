# Translation of kcm_touchpad.po to Catalan (Valencian)
# Copyright (C) 2015-2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2015, 2017, 2018, 2019, 2020, 2021, 2023, 2024.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2017, 2018, 2020, 2022.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:39+0000\n"
"PO-Revision-Date: 2024-02-18 10:44+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#: actions.cpp:19
#, kde-format
msgid "Touchpad"
msgstr "Ratolí tàctil"

#: actions.cpp:22
#, kde-format
msgid "Enable Touchpad"
msgstr "Activa el ratolí tàctil"

#: actions.cpp:30
#, kde-format
msgid "Disable Touchpad"
msgstr "Desactiva el ratolí tàctil"

#: actions.cpp:38
#, kde-format
msgid "Toggle Touchpad"
msgstr "Activa o desactiva el ratolí tàctil"

#: backends/kwin_wayland/kwinwaylandbackend.cpp:59
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""
"La consulta dels dispositius d'entrada ha fallat. Torneu a obrir este mòdul "
"de configuració."

#: backends/kwin_wayland/kwinwaylandbackend.cpp:74
#, kde-format
msgid "Critical error on reading fundamental device infos for touchpad %1."
msgstr ""
"Error crític en llegir la informació fonamental del dispositiu per al ratolí "
"tàctil %1."

#: backends/x11/xlibbackend.cpp:71
#, kde-format
msgid "Cannot connect to X server"
msgstr "No s'ha pogut connectar amb el servidor X"

#: backends/x11/xlibbackend.cpp:84 kcm/libinput/touchpad.qml:95
#, kde-format
msgid "No touchpad found"
msgstr "No s'ha trobat cap ratolí tàctil"

#: backends/x11/xlibbackend.cpp:124 backends/x11/xlibbackend.cpp:138
#, kde-format
msgid "Cannot apply touchpad configuration"
msgstr "No s'ha pogut aplicar la configuració del ratolí tàctil"

#: backends/x11/xlibbackend.cpp:152 backends/x11/xlibbackend.cpp:165
#, kde-format
msgid "Cannot read touchpad configuration"
msgstr "No s'ha pogut llegir la configuració del ratolí tàctil"

#: backends/x11/xlibbackend.cpp:178
#, kde-format
msgid "Cannot read default touchpad configuration"
msgstr "No s'ha pogut llegir la configuració predeterminada del ratolí tàctil"

#: kcm/libinput/touchpad.qml:96
#, kde-format
msgid "Connect an external touchpad"
msgstr "Connecta un ratolí tàctil extern"

#: kcm/libinput/touchpad.qml:110
#, kde-format
msgid "Device:"
msgstr "Dispositiu:"

#: kcm/libinput/touchpad.qml:136
#, kde-format
msgid "General:"
msgstr "General:"

#: kcm/libinput/touchpad.qml:137
#, kde-format
msgid "Device enabled"
msgstr "Dispositiu activat"

#: kcm/libinput/touchpad.qml:141
#, kde-format
msgid "Accept input through this device."
msgstr "Accepta l'entrada a través d'este dispositiu."

#: kcm/libinput/touchpad.qml:165
#, kde-format
msgid "Disable while typing"
msgstr "Desactiva en teclejar"

#: kcm/libinput/touchpad.qml:169
#, kde-format
msgid "Disable touchpad while typing to prevent accidental inputs."
msgstr ""
"Desactiva el ratolí tàctil en teclejar per a evitar entrades accidentals."

#: kcm/libinput/touchpad.qml:193
#, kde-format
msgid "Left handed mode"
msgstr "Mode esquerrà"

#: kcm/libinput/touchpad.qml:197
#, kde-format
msgid "Swap left and right buttons."
msgstr "Intercanvia el botó esquerre i el dret."

#: kcm/libinput/touchpad.qml:221
#, kde-format
msgid "Press left and right buttons for middle click"
msgstr "Prémer els botons esquerre i dret per al clic del mig"

#: kcm/libinput/touchpad.qml:225 kcm/libinput/touchpad.qml:868
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"En fer un clic simultani en el botó dret i esquerre s'envia un clic del botó "
"del mig."

#: kcm/libinput/touchpad.qml:256
#, kde-format
msgid "Pointer speed:"
msgstr "Velocitat del punter:"

#: kcm/libinput/touchpad.qml:354
#, kde-format
msgid "Pointer acceleration:"
msgstr "Acceleració del punter:"

#: kcm/libinput/touchpad.qml:385
#, kde-format
msgid "None"
msgstr "Sense"

#: kcm/libinput/touchpad.qml:389
#, kde-format
msgid "Cursor moves the same distance as finger."
msgstr "El cursor es mou la mateixa distància que el dit."

#: kcm/libinput/touchpad.qml:398
#, kde-format
msgid "Standard"
msgstr "Estàndard"

#: kcm/libinput/touchpad.qml:402
#, kde-format
msgid "Cursor travel distance depends on movement speed of finger."
msgstr "La distància que recorre depén de la velocitat de moviment del dit."

#: kcm/libinput/touchpad.qml:417
#, kde-format
msgid "Tapping:"
msgstr "Tocs:"

#: kcm/libinput/touchpad.qml:418
#, kde-format
msgid "Tap-to-click"
msgstr "Toc per a fer clic"

#: kcm/libinput/touchpad.qml:422
#, kde-format
msgid "Single tap is left button click."
msgstr "Un toc únic és un clic del botó esquerre."

#: kcm/libinput/touchpad.qml:451
#, kde-format
msgid "Tap-and-drag"
msgstr "Tocar i arrossegar"

#: kcm/libinput/touchpad.qml:455
#, kde-format
msgid "Sliding over touchpad directly after tap drags."
msgstr ""
"Arrossega en lliscar directament sobre el ratolí tàctil després d'un toc."

#: kcm/libinput/touchpad.qml:482
#, kde-format
msgid "Tap-and-drag lock"
msgstr "Bloqueig de tocar i arrossegar"

#: kcm/libinput/touchpad.qml:486
#, kde-format
msgid "Dragging continues after a short finger lift."
msgstr "L'arrossegament continua després d'una elevació curta del dit."

#: kcm/libinput/touchpad.qml:506
#, kde-format
msgid "Two-finger tap:"
msgstr "Toc amb dos dits:"

#: kcm/libinput/touchpad.qml:517
#, kde-format
msgid "Right-click (three-finger tap to middle-click)"
msgstr "Clic dret (un toc amb tres dits per al clic del mig)"

#: kcm/libinput/touchpad.qml:518
#, kde-format
msgid ""
"Tap with two fingers to right-click, tap with three fingers to middle-click."
msgstr ""
"Un toc amb dos dits per al clic dret, un toc amb tres dits per al clic del "
"mig."

#: kcm/libinput/touchpad.qml:520
#, kde-format
msgid "Middle-click (three-finger tap right-click)"
msgstr "Clic del mig (un toc amb tres dits per al clic dret)"

#: kcm/libinput/touchpad.qml:521
#, kde-format
msgid ""
"Tap with two fingers to middle-click, tap with three fingers to right-click."
msgstr ""
"Un toc amb dos dits per al clic del mig, un toc amb tres dits per al clic "
"dret."

#: kcm/libinput/touchpad.qml:523
#, kde-format
msgid "Right-click"
msgstr "Clic dret"

#: kcm/libinput/touchpad.qml:524
#, kde-format
msgid "Tap with two fingers to right-click."
msgstr "Un toc amb dos dits per al clic dret."

#: kcm/libinput/touchpad.qml:526
#, kde-format
msgid "Middle-click"
msgstr "Clic del mig"

#: kcm/libinput/touchpad.qml:527
#, kde-format
msgid "Tap with two fingers to middle-click."
msgstr "Un toc amb dos dits per al clic del mig."

#: kcm/libinput/touchpad.qml:586
#, kde-format
msgid "Scrolling:"
msgstr "Desplaçament:"

#: kcm/libinput/touchpad.qml:615
#, kde-format
msgid "Two fingers"
msgstr "Dos dits"

#: kcm/libinput/touchpad.qml:619
#, kde-format
msgid "Slide with two fingers scrolls."
msgstr "Desplaçament en lliscar dos dits."

#: kcm/libinput/touchpad.qml:627
#, kde-format
msgid "Touchpad edges"
msgstr "Vores del ratolí tàctil"

#: kcm/libinput/touchpad.qml:631
#, kde-format
msgid "Slide on the touchpad edges scrolls."
msgstr "Desplaçament en lliscar a les vores del ratolí tàctil."

#: kcm/libinput/touchpad.qml:641
#, kde-format
msgid "Invert scroll direction (Natural scrolling)"
msgstr "Invertix la direcció de desplaçament (desplaçament natural)"

#: kcm/libinput/touchpad.qml:657
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "Pantalla tàctil com el desplaçament."

#: kcm/libinput/touchpad.qml:665 kcm/libinput/touchpad.qml:682
#, kde-format
msgid "Disable horizontal scrolling"
msgstr "Desactiva el desplaçament horitzontal"

#: kcm/libinput/touchpad.qml:690
#, kde-format
msgid "Scrolling speed:"
msgstr "Velocitat de desplaçament:"

#: kcm/libinput/touchpad.qml:741
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Més lenta"

#: kcm/libinput/touchpad.qml:748
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Més ràpida"

#: kcm/libinput/touchpad.qml:759
#, kde-format
msgid "Right-click:"
msgstr "Clic dret:"

#: kcm/libinput/touchpad.qml:793
#, kde-format
msgid "Press bottom-right corner"
msgstr "Premeu el cantó inferior dret"

#: kcm/libinput/touchpad.qml:797
#, kde-format
msgid ""
"Software enabled buttons will be added to bottom portion of your touchpad."
msgstr ""
"Els botons activats per programari s'afegiran a la part inferior del ratolí "
"tàctil."

#: kcm/libinput/touchpad.qml:805
#, kde-format
msgid "Press anywhere with two fingers"
msgstr "Premeu en qualsevol lloc amb dos dits"

#: kcm/libinput/touchpad.qml:809
#, kde-format
msgid "Tap with two finger to enable right click."
msgstr "Un toc amb dos dits per a activar el clic dret."

#: kcm/libinput/touchpad.qml:823
#, kde-format
msgid "Middle-click: "
msgstr "Clic del mig: "

#: kcm/libinput/touchpad.qml:852
#, kde-format
msgid "Press bottom-middle"
msgstr "Premeu la part inferior central"

#: kcm/libinput/touchpad.qml:856
#, kde-format
msgid ""
"Software enabled middle-button will be added to bottom portion of your "
"touchpad."
msgstr ""
"El botó del mig activat per programari s'afegirà a la part inferior del "
"ratolí tàctil."

#: kcm/libinput/touchpad.qml:864
#, kde-format
msgid "Press bottom left and bottom right corners simultaneously"
msgstr ""
"Premeu el cantó inferior esquerre i el cantó inferior dret simultàniament"

#: kcm/libinput/touchpad.qml:877
#, kde-format
msgid "Press anywhere with three fingers"
msgstr "Premeu en qualsevol lloc amb tres dits"

#: kcm/libinput/touchpad.qml:883
#, kde-format
msgid "Press anywhere with three fingers."
msgstr "Premeu en qualsevol lloc amb tres dits."

#: kcm/touchpadconfig.cpp:99
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"S'ha produït un error en carregar els valors. Vegeu els registres per a més "
"informació. Torneu a iniciar este mòdul de configuració."

#: kcm/touchpadconfig.cpp:102
#, kde-format
msgid "No touchpad found. Connect touchpad now."
msgstr "No s'ha trobat cap ratolí tàctil. Connecteu ara el ratolí tàctil."

#: kcm/touchpadconfig.cpp:111
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"No s'han pogut guardar tots els canvis. Vegeu els registres per a més "
"informació. Torneu a iniciar este mòdul de configuració i proveu-ho una "
"altra vegada."

#: kcm/touchpadconfig.cpp:130
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"S'ha produït un error en carregar els valors predeterminats. No s'han pogut "
"establir diverses opcions als seus valors predeterminats."

#: kcm/touchpadconfig.cpp:150
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"S'ha produït un error en afegir un dispositiu connectat nou. Torneu a "
"connectar-lo i reinicieu este mòdul de configuració."

#: kcm/touchpadconfig.cpp:173
#, kde-format
msgid "Touchpad disconnected. Closed its setting dialog."
msgstr ""
"S'ha desconnectat el ratolí tàctil. Es tanca el seu diàleg de configuració."

#: kcm/touchpadconfig.cpp:175
#, kde-format
msgid "Touchpad disconnected. No other touchpads found."
msgstr ""
"S'ha desconnectat el ratolí tàctil. No s'ha trobat cap altre ratolí tàctil."

#: kded/kded.cpp:201
#, kde-format
msgid "Touchpad was disabled because a mouse was plugged in"
msgstr "El ratolí tàctil s'ha desactivat perquè s'ha connectat un ratolí"

#: kded/kded.cpp:204
#, kde-format
msgid "Touchpad was enabled because the mouse was unplugged"
msgstr "El ratolí tàctil s'ha activat perquè s'ha desconnectat el ratolí"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:27
#, kde-format
msgctxt "Emulated mouse button"
msgid "No action"
msgstr "Cap acció"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:30
#, kde-format
msgctxt "Emulated mouse button"
msgid "Left button"
msgstr "Botó esquerre"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:33
#, kde-format
msgctxt "Emulated mouse button"
msgid "Middle button"
msgstr "Botó del mig"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:36
#, kde-format
msgctxt "Emulated mouse button"
msgid "Right button"
msgstr "Botó dret"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:285
#, kde-format
msgctxt "Touchpad Edge"
msgid "All edges"
msgstr "Totes les vores"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:288
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top edge"
msgstr "Vora superior"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:291
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top right corner"
msgstr "Cantó superior dret"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:294
#, kde-format
msgctxt "Touchpad Edge"
msgid "Right edge"
msgstr "Vora dreta"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:297
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom right corner"
msgstr "Cantó inferior dret"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:300
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom edge"
msgstr "Vora inferior"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:303
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom left corner"
msgstr "Cantó inferior esquerre"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:306
#, kde-format
msgctxt "Touchpad Edge"
msgid "Left edge"
msgstr "Vora esquerra"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:309
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top left corner"
msgstr "Cantó superior esquerre"
