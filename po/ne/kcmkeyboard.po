# translation of kcmkeyboard.po to Nepali
# Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>, 2007.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-03 01:37+0000\n"
"PO-Revision-Date: 2007-11-07 15:38+0545\n"
"Last-Translator: Shyam Krishna Bal <shyamkrishna_bal@yahoo.com>\n"
"Language-Team: Nepali <info@mpp.org.np>\n"
"Language: ne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n !=1\n"
"X-Generator: KBabel 1.11.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kapil Timilsina, श्यामकृष्ण बल"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lipak21@gmail.com, shyamkrishna_bal@yahoo.com"

#: bindings.cpp:24
#, fuzzy, kde-format
#| msgid "Switch keyboard layout"
msgid "Keyboard Layout Switcher"
msgstr "कुञ्जीपाटी सजावट स्विच गर्नुहोस्"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "पछिल्लो कुञ्जीपाटी सजावटमा स्विच गर्नुहोस्"

#: bindings.cpp:30
#, fuzzy, kde-format
#| msgid "Switch to Next Keyboard Layout"
msgid "Switch to Last-Used Keyboard Layout"
msgstr "पछिल्लो कुञ्जीपाटी सजावटमा स्विच गर्नुहोस्"

#: bindings.cpp:60
#, fuzzy, kde-format
#| msgid "Error changing keyboard layout to '%1'"
msgid "Switch keyboard layout to %1"
msgstr "'%1' मा कुञ्जीपाटी सजावट परिवर्तन गर्दा त्रुटि"

#: flags.cpp:77
#, kde-format
msgctxt "layout - variant"
msgid "%1 - %2"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QDialog, AddLayoutDialog)
#: kcm_add_layout_dialog.ui:14
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgid "Add Layout"
msgstr "कुञ्जीपाटी दोहोर्याउनुहोस्"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, layoutSearchField)
#: kcm_add_layout_dialog.ui:20
#, kde-format
msgid "Search…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, shortcutLabel)
#: kcm_add_layout_dialog.ui:45
#, kde-format
msgid "Shortcut:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, labelLabel)
#: kcm_add_layout_dialog.ui:55
#, kde-format
msgid "Label:"
msgstr "लेबुल:"

#. i18n: ectx: property (text), widget (QPushButton, prevbutton)
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm_add_layout_dialog.ui:76 kcm_keyboard.ui:341
#, kde-format
msgid "Preview"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tabHardware)
#: kcm_keyboard.ui:21
#, kde-format
msgid "Hardware"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm_keyboard.ui:36
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgid "Keyboard &model:"
msgstr "कुञ्जीपाटी दोहोर्याउनुहोस्"

#. i18n: ectx: property (whatsThis), widget (QComboBox, keyboardModelComboBox)
#: kcm_keyboard.ui:56
#, kde-format
msgid ""
"Here you can choose a keyboard model. This setting is independent of your "
"keyboard layout and refers to the \"hardware\" model, i.e. the way your "
"keyboard is manufactured. Modern keyboards that come with your computer "
"usually have two extra keys and are referred to as \"104-key\" models, which "
"is probably what you want if you do not know what kind of keyboard you "
"have.\n"
msgstr ""
"तपाईँले यहाँ कुञ्जीपाटी नमूना रोज्न सक्नुहुन्छ । यो सेटिङ तपाईँको कुञ्जिपाटी सजावटको "
"स्वतन्त्रता हो र \"हार्डवेयर\" नमूनामा सन्दर्भ गर्छ, जस्तै: तपाईँको कुञ्जीपाटी निर्माण "
"गरिएको तरिका । तपाईँको कम्प्युटरमा आउने आधुनिक कुञ्जीपाटी हो जसमा सधैँ दुइ अतिरिक्त कुञ्जी "
"हुन्छन् र नमूना \"104-key\" सन्दर्भ गर्छन्, यदि तपाईँसँग भएको कुञ्जीपाटी कस्तो प्रकारको हो "
"थाहा नभएमा सम्भवत: यो तपाईँले चाहेको नमूना हुन सक्छ ।\n"

#. i18n: ectx: attribute (title), widget (QWidget, tabLayouts)
#: kcm_keyboard.ui:97
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Layouts"
msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:105
#, kde-format
msgid ""
"If you select \"Application\" or \"Window\" switching policy, changing the "
"keyboard layout will only affect the current application or window."
msgstr ""
"यदि तपाईँ \"अनुप्रयोग\" वा \"सञ्झ्याल\" स्विच नीति चयन गर्नुहुन्छ भने, कुञ्जीपाटी सजावटले "
"हालको अनुप्रयोग वा सञ्झ्याललाई मात्र प्रभाव पार्छ ।"

#. i18n: ectx: property (title), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:108
#, kde-format
msgid "Switching Policy"
msgstr "नीति स्विच गर्दै"

#. i18n: ectx: property (text), widget (QRadioButton, switchByGlobalRadioBtn)
#: kcm_keyboard.ui:114
#, kde-format
msgid "&Global"
msgstr "विश्वव्यापी"

#. i18n: ectx: property (text), widget (QRadioButton, switchByDesktopRadioBtn)
#: kcm_keyboard.ui:127
#, fuzzy, kde-format
#| msgid "Desktop"
msgid "&Desktop"
msgstr "डेस्कटप"

#. i18n: ectx: property (text), widget (QRadioButton, switchByApplicationRadioBtn)
#: kcm_keyboard.ui:137
#, fuzzy, kde-format
#| msgid "Application"
msgid "&Application"
msgstr "अनुप्रयोग"

#. i18n: ectx: property (text), widget (QRadioButton, switchByWindowRadioBtn)
#: kcm_keyboard.ui:147
#, kde-format
msgid "&Window"
msgstr "सञ्झ्याल"

#. i18n: ectx: property (title), widget (QGroupBox, shortcutsGroupBox)
#: kcm_keyboard.ui:160
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Shortcuts for Switching Layout"
msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm_keyboard.ui:166
#, kde-format
msgid "Main shortcuts:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkbGrpShortcutBtn)
#: kcm_keyboard.ui:179
#, kde-format
msgid ""
"This is a shortcut for switching layouts which is handled by X.org. It "
"allows modifier-only shortcuts."
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, xkbGrpShortcutBtn)
#. i18n: ectx: property (text), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:182 kcm_keyboard.ui:212
#, kde-format
msgctxt "no shortcut defined"
msgid "None"
msgstr ""

#. i18n: ectx: property (text), widget (QToolButton, xkbGrpClearBtn)
#. i18n: ectx: property (text), widget (QToolButton, xkb3rdLevelClearBtn)
#: kcm_keyboard.ui:189 kcm_keyboard.ui:219
#, kde-format
msgid "…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm_keyboard.ui:196
#, kde-format
msgid "3rd level shortcuts:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:209
#, kde-format
msgid ""
"This is a shortcut for switching to a third level of the active layout (if "
"it has one) which is handled by X.org. It allows modifier-only shortcuts."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm_keyboard.ui:226
#, kde-format
msgid "Alternative shortcut:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, kdeKeySequence)
#: kcm_keyboard.ui:239
#, kde-format
msgid ""
"This is a shortcut for switching layouts. It does not support modifier-only "
"shortcuts and also may not work in some situations (e.g. if popup is active "
"or from screensaver)."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm_keyboard.ui:249
#, kde-format
msgid "Last used shortcut:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, toggleLastUsedLayoutKeySequence)
#: kcm_keyboard.ui:262
#, kde-format
msgid ""
"This shortcut allows for fast switching between two layouts, by always "
"switching to the last-used one."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_configureLayouts)
#: kcm_keyboard.ui:287
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Configure layouts"
msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#. i18n: ectx: property (text), widget (QPushButton, addLayoutBtn)
#: kcm_keyboard.ui:301
#, kde-format
msgid "Add"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, removeLayoutBtn)
#: kcm_keyboard.ui:311
#, kde-format
msgid "Remove"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, moveUpBtn)
#: kcm_keyboard.ui:321
#, kde-format
msgid "Move Up"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, moveDownBtn)
#: kcm_keyboard.ui:331
#, kde-format
msgid "Move Down"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, layoutLoopingCheckBox)
#: kcm_keyboard.ui:376
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Spare layouts"
msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm_keyboard.ui:408
#, kde-format
msgid "Main layout count:"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tabAdvanced)
#: kcm_keyboard.ui:438
#, kde-format
msgid "Advanced"
msgstr "उन्नत"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_resetOldXkbOptions)
#: kcm_keyboard.ui:444
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "&Configure keyboard options"
msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#: kcm_keyboard_widget.cpp:209
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr ""

#: kcm_keyboard_widget.cpp:211
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr ""

#: kcm_keyboard_widget.cpp:655
#, kde-format
msgctxt "no shortcuts defined"
msgid "None"
msgstr ""

#: kcm_keyboard_widget.cpp:669
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] ""
msgstr[1] ""

#: kcm_view_models.cpp:200
#, fuzzy, kde-format
#| msgid "Map"
msgctxt "layout map name"
msgid "Map"
msgstr "मानचित्र"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Layout"
msgstr "सजावट"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Variant"
msgstr "चल"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Label"
msgstr "लेबुल"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Shortcut"
msgstr ""

#: kcm_view_models.cpp:273
#, kde-format
msgctxt "variant"
msgid "Default"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcmmiscwidget.ui:31
#, kde-format
msgid "When a key is held:"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, accentMenuRadioButton)
#: kcmmiscwidget.ui:38
#, kde-format
msgid "&Show accented and similar characters "
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, repeatRadioButton)
#: kcmmiscwidget.ui:45
#, kde-format
msgid "&Repeat the key"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, nothingRadioButton)
#: kcmmiscwidget.ui:52
#, kde-format
msgid "&Do nothing"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcmmiscwidget.ui:66
#, kde-format
msgid "Test area:"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEdit)
#: kcmmiscwidget.ui:73
#, kde-format
msgid ""
"Allows to test keyboard repeat and click volume (just don't forget to apply "
"the changes)."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:82
#, fuzzy, kde-format
#| msgid ""
#| "If supported, this option allows you to setup the state of NumLock after "
#| "KDE startup.<p>You can configure NumLock to be turned on or off, or "
#| "configure KDE not to set NumLock state."
msgid ""
"If supported, this option allows you to setup the state of NumLock after "
"Plasma startup.<p>You can configure NumLock to be turned on or off, or "
"configure Plasma not to set NumLock state."
msgstr ""
"यदि समर्थित छ भने, यो विकल्पले तपाईँलाई केडीई सुरुआत पछि नमलकको स्थिति सेटअप गर्न अनुमति "
"दिन्छ ।<p>तपाईँ नमलकलाई खोल्न वा बन्द गर्न कन्फिगर गर्न सक्नुहुन्छ, वा केडीईमा नमलक "
"स्थिति सेट नगर्न कन्फिगर गर्न सक्नुहुन्छ ।"

#. i18n: ectx: property (title), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:85
#, fuzzy, kde-format
#| msgid "NumLock on KDE Startup"
msgid "NumLock on Plasma Startup"
msgstr "केडीई सुरुआतमा नमलक"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton1)
#: kcmmiscwidget.ui:97
#, kde-format
msgid "T&urn on"
msgstr "खोल्नुहोस्"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton2)
#: kcmmiscwidget.ui:104
#, fuzzy, kde-format
#| msgid "Turn o&ff"
msgid "&Turn off"
msgstr "बन्द गर्नुहोस्"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton3)
#: kcmmiscwidget.ui:111
#, kde-format
msgid "Leave unchan&ged"
msgstr "अपरिवर्तित छोड्नुहोस्"

#. i18n: ectx: property (text), widget (QLabel, lblRate)
#: kcmmiscwidget.ui:148
#, kde-format
msgid "&Rate:"
msgstr "दर:"

#. i18n: ectx: property (whatsThis), widget (QSlider, delaySlider)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:164 kcmmiscwidget.ui:202
#, kde-format
msgid ""
"If supported, this option allows you to set the delay after which a pressed "
"key will start generating keycodes. The 'Repeat rate' option controls the "
"frequency of these keycodes."
msgstr ""
"यदि समर्थित छ भने, यो विकल्पले तपाईँलाई थिचिएको कुञ्जीले कुञ्जी कोडहरू सिर्जना गर्न सुरु "
"गरेपछि विलम्ब सेट गर्न अनुमति दिन्छ । 'दोहोर्याइ दर' विकल्पले यी कुञ्जी कोडहरूको "
"सोपानुक्रम नियन्त्रेण गर्दछ ।"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, kcfg_repeatRate)
#. i18n: ectx: property (whatsThis), widget (QSlider, rateSlider)
#: kcmmiscwidget.ui:192 kcmmiscwidget.ui:212
#, kde-format
msgid ""
"If supported, this option allows you to set the rate at which keycodes are "
"generated while a key is pressed."
msgstr ""
"यदि समर्थित छ भने, यो विकल्पले तपाईँलाई कुञ्जी थिच्दा कुञ्जी कोडहरू सिर्जना भएको दर सेट "
"गर्न अनुमति दिन्छ ।"

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_repeatRate)
#: kcmmiscwidget.ui:195
#, kde-format
msgid " repeats/s"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:205
#, kde-format
msgid " ms"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, lblDelay)
#: kcmmiscwidget.ui:246
#, kde-format
msgid "&Delay:"
msgstr "विलम्ब:"

#: tastenbrett/main.cpp:52
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "कुञ्जीपाटी दोहोर्याउनुहोस्"

#: tastenbrett/main.cpp:54
#, fuzzy, kde-format
#| msgid "Switch keyboard layout"
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "कुञ्जीपाटी सजावट स्विच गर्नुहोस्"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant."
msgstr ""

#, fuzzy
#~| msgid "KDE Keyboard Layout Switcher"
#~ msgid "KDE Keyboard Control Module"
#~ msgstr "केडीई कुञ्जीपाटी सजावट स्विचकर्ता"

#, fuzzy
#~| msgid "Copyright (C) 2006-2007 Andriy Rysin"
#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "Copyright (C) 2006-2007 एन्ड्रि राइसिन"

#, fuzzy
#~| msgid "Switch keyboard layout"
#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "कुञ्जीपाटी सजावट स्विच गर्नुहोस्"

#~ msgid "..."
#~ msgstr "..."

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Show for single layout"
#~ msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#, fuzzy
#~| msgid "Keyboard Repeat"
#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "कुञ्जीपाटी दोहोर्याउनुहोस्"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Configure Layouts..."
#~ msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#~ msgid "Keyboard Repeat"
#~ msgstr "कुञ्जीपाटी दोहोर्याउनुहोस्"

#~ msgid "Turn o&ff"
#~ msgstr "बन्द गर्नुहोस्"

#, fuzzy
#~| msgid "Leave unchan&ged"
#~ msgid "&Leave unchanged"
#~ msgstr "अपरिवर्तित छोड्नुहोस्"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Configure..."
#~ msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"

#~ msgid ""
#~ "If supported, this option allows you to hear audible clicks from your "
#~ "computer's speakers when you press the keys on your keyboard. This might "
#~ "be useful if your keyboard does not have mechanical keys, or if the sound "
#~ "that the keys make is very soft.<p>You can change the loudness of the key "
#~ "click feedback by dragging the slider button or by clicking the up/down "
#~ "arrows on the spin box. Setting the volume to 0% turns off the key click."
#~ msgstr ""
#~ "यदि समर्थित छ भने, यो विकल्पले तपाईँलाई आफ्नो कुञ्जीपाटीमा कुञ्जीहरू थिच्दा तपाईँको "
#~ "कम्प्युटरको स्पिकरबाट श्रब्य क्लिकहरू सुन्न अनुमति दिन्छ । यदि तपाईँको कुञ्जीपाटीसँग "
#~ "यांत्रिक कुञ्जीहरू छन् भने, वा यदि ध्वनिलाई कुञ्जीले धेरै नरम बनाएको छ भने यो उपयोगी हुन "
#~ "सक्छ ।<p> तपाईँ स्लाइडर बटन तानेर कुञ्जी वा स्पिन बाकसमा माथिल्लो/तल्लो बाँण क्लिक "
#~ "गरेर कुञ्जी क्लिक पृष्ठपोषणको उच्च आवाज परिवर्तन गर्न सक्नुहुन्छ । 0% मा भोल्युम सेट "
#~ "गर्नाले कुञ्जी क्लिक बन्द हुन्छ ।"

#, fuzzy
#~| msgid "Key click &volume:"
#~ msgid "&Key click volume:"
#~ msgstr "कुञ्जी क्लिक भोल्युम:"

#~ msgid ""
#~ "If you check this option, pressing and holding down a key emits the same "
#~ "character over and over again. For example, pressing and holding down the "
#~ "Tab key will have the same effect as that of pressing that key several "
#~ "times in succession: Tab characters continue to be emitted until you "
#~ "release the key."
#~ msgstr ""
#~ "यदि तपाईँले यो विकल्प जाँच गर्नुभयो भने, कुञ्जी थिच्दा वा समात्दा एउटै क्यारेक्टर माथि "
#~ "दोहोर्याइ तेहर्याइ छोड्दछ । उदाहरणका लागि, ट्याब कुञ्जी थिच्दा वा समात्दा उही "
#~ "प्रभाव पर्नेछ जुन सो कुञ्जी विभिन्न पटक थिच्दा अनुक्रममा हुन्छ । तपाईँले कुञ्जी नछोडे सम्म "
#~ "ट्याब क्यारेक्टरले निकालिन जारी राख्दछ ।"

#~ msgid "&Enable keyboard repeat"
#~ msgstr "कुञ्जीपाटी दोहोर्याइ सक्षम पार्नुहोस्"
